//API CLASE
import express from 'express';
let app = new express();

function saludar(peticion, respuesta){
    respuesta.send("HOLA API!!");
}
//Ruta
app.get('/saludo',saludar);

function calcular(req, res){
    let miSumador = new Sumador();
    let resultado = miSumador.sumar(56,76);
    res.send(resultado.toString());
}
//Ruta
app.get('/calculadora',calcular);

function aleatorio(requi, respu){
    let miAleatorio = new Aleatorio();
    let calculo = miAleatorio.calcularaleatorio(1,100);
    respu.send(calculo.toString());
}
//Ruta
app.get('/aleatorio',aleatorio);
//Donde la API escucha
app.listen(3000);

//Clase para sumar
class Sumador{
    sumar(a,b){
        return a+b;
    }
}

//Clase para generar numero aleatorio
class Aleatorio{
    a;
    calcularaleatorio(){
        this.a = Math.random(Math.random());
        return this.a;
    }
}